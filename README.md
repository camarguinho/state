STATE: Em certas ocasiões, quando o contexto em que está a desenvolver requer um objeto que possua comportamentos diferentes dependendo de qual estado se encontra, é difícil manejar a mudança de comportamento e os estados desse objeto, tudo dentro do mesmo bloco de código. O padrão State propõe uma solução para esta complicação, criando basicamente, um objeto para cada estado possível do objeto que o chama. Assim, permite que um objeto altere seu comportamento de acordo com o estado interno que se encontra em um momento dado.

Para testar o padrão, execute  classe TesteDeDescontoExtra.
