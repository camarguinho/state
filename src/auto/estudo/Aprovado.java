package auto.estudo;

public class Aprovado implements EstadoOrcamento {

	@Override
	public void aplicaDesconto(Orcamento orcamento) {
		orcamento.valor -= orcamento.getValor() * 0.02;
		orcamento.descontoAplicado  = true;
	}

	@Override
	public void aprovar(Orcamento orcamento) {
		throw new RuntimeException("Já está aprovado!");
	}

	@Override
	public void reprova(Orcamento orcamento) {
		throw new RuntimeException("Não pode ser reprovado!");
	}

	@Override
	public void finaliza(Orcamento orcamento) {
		orcamento.estadoAtual = new Finalizado();
	}

}
