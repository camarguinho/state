package auto.estudo;

public class EmAprovacao implements EstadoOrcamento {

	@Override
	public void aplicaDesconto(Orcamento orcamento) {
		orcamento.valor -= orcamento.valor * 0.05;
		orcamento.descontoAplicado  = true;
	}

	@Override
	public void aprovar(Orcamento orcamento) {
		orcamento.estadoAtual = new Aprovado();
	}

	@Override
	public void reprova(Orcamento orcamento) {
		orcamento.estadoAtual = new Reprovado();
	}

	@Override
	public void finaliza(Orcamento orcamento) {
		throw new RuntimeException("Não pode ser finalizado!");		
	}

}
