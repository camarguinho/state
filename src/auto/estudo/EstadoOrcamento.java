package auto.estudo;

public interface EstadoOrcamento {

	void aplicaDesconto(Orcamento orcamento);
	void aprovar(Orcamento orcamento);
	void reprova(Orcamento orcamento);
	void finaliza(Orcamento orcamento);
}
