package auto.estudo;

public class Finalizado implements EstadoOrcamento {

	@Override
	public void aplicaDesconto(Orcamento orcamento) {
		throw new RuntimeException("Desconto extra não vale para orçamento finalizado!");	
	}

	@Override
	public void aprovar(Orcamento orcamento) {
		throw new RuntimeException("Não pode ser finalizado!"); 
	}

	@Override
	public void reprova(Orcamento orcamento) {
		throw new RuntimeException("Não pode ser reprovado!");
	}

	@Override
	public void finaliza(Orcamento orcamento) {
		throw new RuntimeException("Não pode ser finalizado!");		
	}

}
