package auto.estudo;

import java.util.ArrayList;
import java.util.List;

public class Orcamento {

	protected double valor;
	private final List<Item> itens;
	protected EstadoOrcamento estadoAtual;
	protected boolean descontoAplicado;
	
	public Orcamento(double valor){
		this.valor = valor;
		itens = new ArrayList<Item>();
		estadoAtual = new EmAprovacao();
	}

	public double getValor() {
		return valor;
	}

	public List<Item> getItens() {
		return java.util.Collections.unmodifiableList(itens);
	}

	public void adicionaItem(Item item){
		itens.add(item);
	}

	public void aplicaDesconto() {
		if(descontoAplicado == false){
			estadoAtual.aplicaDesconto(this);
		} else {
			throw new RuntimeException("Desconto já aplicado");
		}
	}
	
	public void aprova(){
		estadoAtual.aprovar(this);
	}
	
	public void reprova(){
		estadoAtual.reprova(this);
	}
	
	public void finaliza(){
		estadoAtual.finaliza(this);
	}
}
