package auto.estudo;

public class Reprovado implements EstadoOrcamento {

	@Override
	public void aplicaDesconto(Orcamento orcamento) {
		throw new RuntimeException("Desconto extra não vale para orçamento reprovado!");
	}

	@Override
	public void aprovar(Orcamento orcamento) {
		throw new RuntimeException("Não pode ser aprovado!");		
	}

	@Override
	public void reprova(Orcamento orcamento) {
		throw new RuntimeException("Já está reprovado!");
	}

	@Override
	public void finaliza(Orcamento orcamento) {
		orcamento.estadoAtual = new Finalizado();		
	}

}