package auto.estudo;

public class TesteDeDescontoExtra {

	public static void main(String[] args) {
		Orcamento orcamento = new Orcamento(500);
		orcamento.aplicaDesconto();
		System.out.println(orcamento.getValor());
		
		orcamento.aprova();
		orcamento.aplicaDesconto();
		System.out.println(orcamento.getValor());
		
		orcamento.reprova();
		orcamento.aplicaDesconto();
		System.out.println(orcamento.getValor());
		
	}
	
}
